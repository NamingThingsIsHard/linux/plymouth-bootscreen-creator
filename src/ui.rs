use gdk_pixbuf::{InterpType, PixbufLoader, PixbufLoaderExt};
use gio::FileExt;
use gtk::prelude::BuilderExtManual;
use gtk::{
    AssistantExt, Builder, Button, ButtonExt, EditableSignals, Entry, EntryExt, FileChooserButton,
    FileChooserButtonExt, FileChooserExt, GtkWindowExt, Image, ImageExt, Widget, WidgetExt,
};

use crate::ffmpeg::get_video_thumbnail;
use crate::make_id_for_filesystem;
use crate::plymouth::{activate_theme, generate, install, preview, uninstall};

const MINIMAL_NAME_LENGTH: usize = 3;

/// Reduces boilerplate when getting the text as a &str from a gtk::Entry
///
/// ```no_run
/// entry_str!(text_var_name, entry_object)
/// ```
macro_rules! entry_str {
    ($var:ident, $entry: expr) => {
        let gstring = $entry.get_text();
        let $var = gstring.as_str();
    };
}

pub fn init_main_window(builder: &Builder) {
    let assistant: gtk::Assistant = builder.get_object("window_main").unwrap();

    // Make sure we can exit
    assistant.connect_cancel(|_| gtk::main_quit());

    // Set the main icon
    let load_icon = || -> Result<(), glib::Error> {
        let loader = PixbufLoader::with_type("png")?;
        loader.write(include_bytes!("resources/images/icon_16x16.png"))?;
        loader.close()?;
        assistant.set_icon(loader.get_pixbuf().as_ref());
        Ok(())
    };
    if let Err(err) = load_icon() {
        eprintln!("Error loading window icon: {}", err.to_string());
    }
}

pub fn connect_step_info(builder: &Builder) {
    let assistant: gtk::Assistant = builder.get_object("window_main").unwrap();

    let step__info: Widget = builder.get_object("step__info").unwrap();
    let entry__name: Entry = builder.get_object("entry__name").unwrap();
    let entry__id: Entry = builder.get_object("entry__id").unwrap();

    // Handle name entry to generate ID
    entry__name.connect_changed(move |entry| {
        let name = entry.get_text();
        entry__id.set_text(make_id_for_filesystem(name.as_str()).as_str());

        // Name is the only mandatory field
        assistant.set_page_complete(&step__info, name.len() > MINIMAL_NAME_LENGTH);
    });
}

/// Connects the signals of the internal widgets so that the interface actually does something
pub fn connect_step_video(builder: &Builder) {
    let assistant: gtk::Assistant = builder.get_object("window_main").unwrap();

    let step__video: Widget = builder.get_object("step__video").unwrap();
    let chooser__video: FileChooserButton = builder.get_object("chooser__video").unwrap();
    let label__thumbnail: Image = builder.get_object("label__thumbnail").unwrap();
    chooser__video.connect_file_set(move |chooser| {
        let attempt = || -> Result<(), &str> {
            let file = chooser.get_file().ok_or("No file set")?;
            let path = file.get_path().ok_or("No path set")?;
            let file_path = path.to_str().ok_or("Couldn't get set path")?;
            let thumbnail = get_video_thumbnail(file_path).ok_or("Error generating thumbnail")?;
            let size = label__thumbnail.get_allocation();
            // TODO: Scale to preserve aspect ratio
            let _thumbnail = thumbnail.scale_simple(size.width, size.height, InterpType::Hyper);
            label__thumbnail.set_from_pixbuf(Option::from(&_thumbnail));

            // Allow moving to the next step
            assistant.set_page_complete(&step__video, true);
            Ok(())
        };
        if let Err(message) = attempt() {
            assistant.set_page_complete(&step__video, false);
            eprintln!("Couldn't set file: {}", message)
        }
    });
}

pub fn connect_step_generation(builder: &Builder, temporary_path: String) {
    let chooser__video: FileChooserButton = builder.get_object("chooser__video").unwrap();
    let button__install: Button = builder.get_object("button__install").unwrap();
    let button__preview: Button = builder.get_object("button__preview").unwrap();
    let entry__name: Entry = builder.get_object("entry__name").unwrap();
    let entry__id: Entry = builder.get_object("entry__id").unwrap();
    let entry__description: Entry = builder.get_object("entry__description").unwrap();

    button__preview.hide();

    button__install.connect_clicked(move |_button| {
        entry_str!(name, entry__name);
        entry_str!(id, entry__id);
        entry_str!(desc, entry__description);
        let gen_result = generate(
            name,
            id,
            desc,
            chooser__video
                .get_file()
                .unwrap()
                .get_path()
                .unwrap()
                .as_path(),
            temporary_path.as_str(),
        );
        if let Err(_message) = gen_result {
            todo!("Show popup with the error and unset completion");
        }
        let install_result = install(id, &temporary_path, None);
        if let Err(_message) = install_result {
            todo!("Show popup with the error and unset completion");
        }

        button__preview.show();
    });
}

pub fn connect_step_preview(builder: &Builder) {
    let assistant: gtk::Assistant = builder.get_object("window_main").unwrap();
    let button__preview: Button = builder.get_object("button__preview").unwrap();
    let entry__id: Entry = builder.get_object("entry__id").unwrap();
    let step__generation: Widget = builder.get_object("step__generation").unwrap();

    button__preview.connect_clicked(move |_button| {
        entry_str!(id, entry__id);
        let preview_result = preview(id, None, None, None);
        if let Err(_message) = preview_result {
            eprintln!("There was an error previewing: {}", _message);
            // todo!("Show popup with the error and unset completion");
        }
        assistant.set_page_complete(&step__generation, true);
    });
}

pub fn connect_step_final(builder: &Builder) {
    let button__done: Button = builder.get_object("button__done").unwrap();
    button__done.connect_clicked(move |_button| gtk::main_quit());

    // Use separate methods in order to use entry__id in different closures
    connect_final__apply(builder);
    connect_final__discard_and_uninstall(builder);
}

/// Make the final step->apply button do stuff
fn connect_final__apply(builder: &Builder) {
    let button__apply: Button = builder.get_object("button__apply").unwrap();
    let entry__id: Entry = builder.get_object("entry__id").unwrap();
    button__apply.connect_clicked(move |_button| {
        let id_gstring = entry__id.get_text();
        let id = id_gstring.to_string();

        let result = activate_theme(&id, true);
        if let Err(err) = result {
            eprintln!("Couldn't activate theme {}: {}", id, err.to_string());
        } else {
            println!("Activated theme. Closing");
            gtk::main_quit()
        }
    });
}

/// Make the final step->"discard and uninstall" button do stuff
fn connect_final__discard_and_uninstall(builder: &Builder) {
    let entry__id: Entry = builder.get_object("entry__id").unwrap();
    let button__discard_and_uninstall: Button =
        builder.get_object("button__discard_and_uninstall").unwrap();
    button__discard_and_uninstall.connect_clicked(move |_button| {
        let id_gstring = entry__id.get_text();
        let id = id_gstring.to_string();
        let result = uninstall(&id, None);
        if let Err(err) = result {
            eprintln!("Couldn't uninstall theme {}: {}", id, err.to_string());
        } else {
            println!("Activated theme. Closing");
            gtk::main_quit()
        }
    });
}
