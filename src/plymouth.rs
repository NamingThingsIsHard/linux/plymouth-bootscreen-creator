use std::fs;
use std::os::unix::fs::PermissionsExt;
use std::path::Path;
use std::process::Command;
use std::thread::sleep;
use std::time::Duration;

use dircpy::copy_dir;
use handlebars::Handlebars;
use serde_json::json;

use crate::ffmpeg;

const PLYMOUTH_THEMES_ROOT: &str = "/usr/share/plymouth/themes";
const XEPHYR_DISPLAY: &str = ":2";
/// Generate the theme in a temporary directory
///
/// returns:
///   - success: The path to that directory
///   - error: The error message
pub fn generate<'a>(
    name: &str,
    id: &str,
    description: &str,
    video: &Path,
    temp_path: &str,
) -> Result<(), &'a str> {
    let temp_dir = Path::new(temp_path);

    // Clean up the animation dir
    let animation_dir = temp_dir.join("animation");
    if animation_dir.exists() {
        fs::remove_dir_all(&animation_dir)
            .map_err(|_| {
                format!(
                    "Couldn't delete animation directory: {}",
                    animation_dir.to_str().unwrap()
                )
            })
            .unwrap();
    }
    fs::create_dir_all(&animation_dir)
        .or(Err("Couldn't create animation directory"))
        .unwrap();

    // Explode video into animation dir
    let animation_pth = animation_dir.as_path();
    let frame_count = ffmpeg::explode(video, animation_pth)?;

    // Render templates
    let bars = Handlebars::new();
    let theme_rendered = bars
        .render_template(
            include_str!("resources/templates/theme.plymouth.mustache"),
            &json!({"Description": description, "ThemeName": name, "ThemeID": id}),
        )
        .or(Err("Theme file templating error"))
        .unwrap();
    let script_rendered = bars
        .render_template(
            include_str!("resources/templates/theme.script.mustache"),
            &json!({ "FrameCount": frame_count }),
        )
        .or(Err("Theme file templating error"))
        .unwrap();

    // Write templates into the target dir
    let theme_plymouth = temp_dir.join(format!("{}.plymouth", id));
    fs::write(theme_plymouth.as_path(), theme_rendered)
        .map_err(|err| format!("Error writing theme template: {}", err))
        .unwrap();
    let theme_script = temp_dir.join(format!("{}.script", id));
    fs::write(theme_script.as_path(), script_rendered)
        .map_err(|err| format!("Error writing theme script: {}", err))
        .unwrap();

    // Make sure script is executable
    let meta__script = fs::metadata(theme_script.as_path()).unwrap();
    let mut perms__exec = meta__script.permissions();
    perms__exec.set_mode(0o744);
    fs::set_permissions(theme_script.as_path(), perms__exec).unwrap();

    Ok(())
}

/// Install theme from temporary directory into plymouth themes directory
pub fn install<'a>(
    id: &str,
    source_dir: &str,
    target_root: Option<&str>,
) -> Result<String, &'a str> {
    let real_root = target_root.unwrap_or(PLYMOUTH_THEMES_ROOT);
    let target_path = Path::new(real_root).to_path_buf().join(id);
    // Clean dir before copy
    if target_path.is_dir() {
        fs::remove_dir_all(&target_path).unwrap();
    }
    let source_path = Path::new(source_dir);
    copy_dir(source_path, &target_path.as_path()).unwrap();
    let output_string = target_path.as_path().to_str().unwrap().to_owned();
    Ok(output_string)
}

pub fn uninstall(id: &str, target_root: Option<&str>) -> std::io::Result<()> {
    let real_root = target_root.unwrap_or(PLYMOUTH_THEMES_ROOT);
    let target_path = Path::new(real_root).to_path_buf().join(id);
    // Clean dir before copy
    if target_path.is_dir() {
        fs::remove_dir_all(&target_path)?;
    }
    Ok(())
}

/// Preview the installed theme with the given ID
pub fn preview(
    id: &str,
    seconds_in: Option<usize>,
    width_in: Option<usize>,
    height_in: Option<usize>,
) -> Result<(), String> {
    let active_theme = get_active_theme()?;
    let seconds = seconds_in.unwrap_or(10);
    let width = width_in.unwrap_or(1280);
    let height = height_in.unwrap_or(720);

    // Make sure Xephyr has a display to use
    #[allow(clippy::or_fun_call)]
    let display = std::env::var("DISPLAY").unwrap_or(String::from(":0"));
    std::env::set_var("DISPLAY", display);
    let mut xephyr = Command::new("Xephyr")
        .args(&[
            XEPHYR_DISPLAY,
            "-screen",
            format!("{}x{}", width, height).as_str(),
        ])
        .spawn()
        .map_err(|err| format!("Couldn't start xephyr: {}", err.to_string()))?;
    // Point X11 programs to the Xephyr window
    std::env::set_var("DISPLAY", XEPHYR_DISPLAY);
    let mut plymouthd = Command::new("plymouthd")
        .args(&["--no-daemon", "--debug"])
        .spawn()
        .map_err(|err| format!("Couldn't start plymouthd: {}", err.to_string()))?;

    let fallible = (|| -> Result<(), String> {
        activate_theme(id, false)
            .map_err(|err| format!("Couldn't set theme: {}", err.to_string()))?;
        // Try to start the process of showing the splash
        let mut try_count = 0;
        while try_count < 5 {
            println!("Showing splash: {}", id);
            if let Ok(status) = Command::new("plymouth").arg("--show-splash").status() {
                if status.success() {
                    break;
                }
            }
            try_count += 1;
            println!("Retry to show splash... {}", try_count);
            sleep(Duration::from_secs(1));
        }
        if try_count >= 5 {
            return Err(String::from("Couldn't start plymouth to show splash"));
        }
        for second in 0..seconds {
            sleep(Duration::from_secs(1));
            assert!(Command::new("plymouth")
                .arg(format!("--update=test{}", second))
                .status()
                .map_err(|err| format!("Error updating plymouth: {}", err.to_string()))?
                .success());
        }
        Ok(())
    })();

    xephyr.kill().ok();
    plymouthd.kill().ok();

    if let Err(err) = activate_theme(&active_theme, false) {
        eprintln!("Couldn't reset the active theme: {}", err.to_string());
    }

    fallible
}

/// Get the bootscreen theme that is currently being activated
pub fn get_active_theme() -> Result<String, String> {
    let output = Command::new("plymouth-set-default-theme")
        .output()
        .map_err(|err| format!("Couldn't execute command: {}", err.to_string()))?;
    let string_result = String::from_utf8(output.stdout);
    if let Err(err) = string_result {
        return Err(format!("Couldn't convert stdout to string: {}", err));
    }
    let string = string_result.unwrap().trim().to_string();
    if string.is_empty() {
        return Err(String::from("No output"));
    }
    println!("Active theme: {}", string);
    Ok(string)
}

/// Sets the theme and activates it by rebuilding initrd
pub fn activate_theme(id: &str, rebuild: bool) -> std::io::Result<()> {
    let mut command = Command::new("plymouth-set-default-theme");
    if rebuild {
        command.arg("--rebuild-initrd");
    }
    let output = command.arg(id).output()?;

    if !output.status.success() {
        #[allow(clippy::or_fun_call)]
        Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            String::from_utf8(output.stderr).unwrap_or(String::from("Unknown error")),
        ))
    } else {
        println!("Activated theme: {}", id);
        Ok(())
    }
}
