extern crate gio;
extern crate glib;
extern crate gtk;
extern crate libc;

use gtk::prelude::*;
use gtk::Builder;

use plymouth_bootscreen_creator::ui;

#[allow(clippy::or_fun_call)]
fn main() -> Result<(), &'static str> {
    // Make sure DISPLAY is set
    std::env::set_var(
        "DISPLAY",
        std::env::var("DISPLAY").unwrap_or_else(|_| String::from(":0")),
    );
    if gtk::init().is_err() {
        return Err("Failed to initialize GTK!");
    }
    // Use https://gitlab.com/dns2utf8/sudo.rs/-/blob/main/src/lib.rs#L88
    println!("Effective uid: {}", users::get_effective_uid());
    match (users::get_current_uid(), users::get_effective_uid()) {
        (0, 0) => {
            println!("Running as root");
        }
        (_, 0) => {
            println!("Switching to root using setuid");
            unsafe {
                libc::setuid(0);
            }
        }
        (_, _) => {
            return Err("Must run pbc as root!");
        }
    }
    extend_env_path();
    println!(
        "Current PATH: {}",
        std::env::var("PATH").unwrap_or(String::from("UNKNOWN"))
    );
    let temporary_dir = temp_dir::TempDir::new().unwrap();
    let glade_src = include_str!("../resources/main.glade");
    let builder = Builder::from_string(glade_src);
    ui::init_main_window(&builder);
    ui::connect_step_info(&builder);
    ui::connect_step_video(&builder);
    ui::connect_step_generation(
        &builder,
        String::from(temporary_dir.path().to_str().unwrap()),
    );
    ui::connect_step_preview(&builder);
    ui::connect_step_final(&builder);
    let assistant: gtk::Assistant = builder.get_object("window_main").unwrap();
    assistant.show_all();

    gtk::main();
    Ok(())
}

/// The environment PATH might not include the necessary directory to find plymouthd
fn extend_env_path() {
    #[allow(clippy::or_fun_call)]
    let path = std::env::var("PATH").unwrap_or(String::from(""));
    let sbin_path = String::from("/usr/sbin");
    if !path.contains(&sbin_path) {
        std::env::set_var("PATH", [sbin_path, path].join(":"))
    }
}
