use gio::prelude::{Cast, ObjectExt};
use gtk::{Container, ContainerExt, Widget};
use regex::Regex;

pub mod ffmpeg;
pub mod plymouth;
pub mod ui;

pub fn print_children(widget: &impl ContainerExt, spacer: &str, depth: i32) {
    for child in widget.get_children() {
        println!("{}Child: {}", spacer, child.get_type().name().as_str(),);
        print_properties(&child, spacer, depth);
        let result = child.clone().upcast::<Widget>().downcast::<Container>();
        if let Ok(container) = result {
            print_children(&container, spacer, depth + 1);
        }
    }
}

pub fn print_properties(object: &impl ObjectExt, spacer: &str, depth: i32) {
    for property in object.list_properties() {
        let prop_name = property.get_name();

        // Try and get value as string
        if let Ok(prop_val) = object.get_property(prop_name.as_str()) {
            let str_res = prop_val.get::<String>();
            if let Ok(Some(value_str)) = str_res {
                println!(
                    "{}- {}: {}",
                    spacer.repeat(depth as usize),
                    prop_name,
                    value_str
                );
            }
        }
    }
}

/// Build ID that can be used for the folder name in the plymouth theme folder
pub fn make_id_for_filesystem(name: &str) -> String {
    let name_to_id_regex: Regex = Regex::new(r"\W+").unwrap();
    name_to_id_regex.replace_all(name, "-").into_owned()
}
