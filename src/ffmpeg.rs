use std::process::Command;
use std::str;

use gdk_pixbuf::{Pixbuf, PixbufLoader, PixbufLoaderExt};
use std::path::{Path, PathBuf};

///
/// https://trac.ffmpeg.org/wiki/Create%20a%20thumbnail%20image%20every%20X%20seconds%20of%20the%20video
/// https://ottverse.com/thumbnails-screenshots-using-ffmpeg/
/// https://doc.rust-lang.org/book/ch11-03-test-organization.html
pub fn get_video_thumbnail(filepath: &str) -> Option<Pixbuf> {
    let mut command = Command::new("ffmpeg");
    let command_with_args = command.args(&[
        "-ss",
        "00:00:01.000",
        "-i",
        filepath,
        "-vframes",
        "1",
        "-f",
        "image2",
        "pipe:",
    ]);
    let command_result = command_with_args.output();
    if command_result.is_err() {
        todo!("Handle command error");
        // return None;
    }
    let command_out = command_result.unwrap();
    let data = command_out.stdout;
    if data.is_empty() {
        println!(
            "stderr: {}",
            str::from_utf8(command_out.stderr.as_slice()).unwrap()
        );
        todo!("Handle no data");
    }
    let loader_result = PixbufLoader::with_type("jpeg");
    if loader_result.is_err() {
        todo!("Handle pixbuf creation error");
    }
    let loader = loader_result.unwrap();
    loader.write(data.as_slice()).unwrap();
    loader.close().unwrap();
    loader.get_pixbuf()
}

/// Explodes a video file into a bunch of pngs within the target directory
///
/// * returns: number of extracted frames (pngs in the dir)
pub fn explode<'a>(video: &Path, target_dir: &Path) -> Result<usize, &'a str> {
    let status = Command::new("ffmpeg")
        .args(&[
            "-hide_banner",
            "-i",
            video.to_str().unwrap(),
            target_dir.join("progress-%01d.png").to_str().unwrap(),
        ])
        .status()
        .unwrap();
    match status.success() {
        true => Ok(target_dir
            .read_dir()
            .unwrap()
            .map(|f| f.unwrap().path())
            .filter(is_png_file)
            .count()),
        false => Err("Error exploding video"),
    }
}

#[allow(clippy::ptr_arg)]
pub fn is_png_file(path: &PathBuf) -> bool {
    path.is_file() && path.extension().unwrap() == "png"
}
