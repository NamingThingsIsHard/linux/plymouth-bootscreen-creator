use std::fs;
use std::os::unix::fs::PermissionsExt;
use std::path::Path;

use plymouth_bootscreen_creator::plymouth;

mod common;
#[test]
fn test_generate() {
    common::setup();
    let name = "Test name";
    let id = "test-name";
    let description = "";

    let video = Path::new("tests/resources/video.mp4");
    let output_dir = temp_dir::TempDir::new().unwrap();
    let output_path = output_dir.path();
    let res = plymouth::generate(
        name,
        id,
        description,
        video,
        &String::from(output_path.to_str().unwrap()),
    );
    assert!(res.is_ok());

    // Check output dir
    assert!(output_dir.child("animation").is_dir());
    assert!(output_dir.child(format!("{}.script", id)).is_file());

    // Make sure script is executable
    let script_path = output_dir.child(format!("{}.plymouth", id));
    assert!(script_path.is_file());
    let mode = script_path.metadata().unwrap().permissions().mode();
    assert_eq!(mode & 0o400, 0o400);
}

#[test]
fn test_install() {
    common::setup();
    let filename = "a_file";
    let id = "an-id";

    let source_dir = temp_dir::TempDir::new().unwrap();
    let source_file = source_dir.path().join(filename);
    fs::write(source_file, "This is a file").unwrap();

    let target_dir = temp_dir::TempDir::new().unwrap();
    let expected_target_file = target_dir.path().join(id).join(filename);
    let res = plymouth::install(
        id,
        &String::from(source_dir.path().to_str().unwrap()),
        Some(target_dir.path().to_str().unwrap()),
    );
    assert!(res.is_ok());
    assert!(expected_target_file.is_file());
}
