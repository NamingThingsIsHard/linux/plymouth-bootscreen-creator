pub fn setup() {
    // Make sure to use shm for tests
    std::env::set_var("TMPDIR", "/dev/shm");
}
