use std::path::Path;

use plymouth_bootscreen_creator::ffmpeg;

mod common;
const VIDEO_PATH: &str = "tests/resources/video.mp4";
#[test]
fn test_explode() {
    common::setup();
    let animation_dir = temp_dir::TempDir::new().unwrap();
    let video = Path::new(VIDEO_PATH);
    let frame_count = ffmpeg::explode(video, animation_dir.path()).unwrap();
    assert_eq!(frame_count, 76);
}
