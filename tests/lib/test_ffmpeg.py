"""
plymouth-bootscreen-creator
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import unittest
from pathlib import Path

from pbc.lib.ffmpeg import DictWrapper, get_video_info

RESOURCES = Path(__file__).parent.parent / "resources"


class DictWrapperTest(unittest.TestCase):
    """Test the DictWrapper class"""

    def setUp(self):
        self.fps = 30
        self.frame_count = 100
        self.resolution = "1920x1080"
        self.info = DictWrapper(
            {
                "fps": self.fps,
                "frame_count": self.frame_count,
                "resolution": self.resolution,
            }
        )

    def test_attr_access(self):
        """Using an attribute to get keys with the same name in the internal dict should work"""
        self.assertEqual(self.info.fps, self.fps)
        self.assertEqual(self.info.frame_count, self.frame_count)
        self.assertEqual(self.info.resolution, self.resolution)

    def test_attr__fail(self):
        """Attempting to get an attribute with no corresponding key in internal dict should fail"""

        def call():
            return DictWrapper({}).bad

        self.assertRaises(AttributeError, call)


class VideoInfoGetTest(unittest.TestCase):
    """Tests related to getting video information from a test file"""

    def test_get(self):
        """Ensure we can get correct video information"""
        info = get_video_info(RESOURCES / "video.mp4")

        self.assertEqual(info.fps, 30)
        self.assertGreater(info.frame_count, 0)
        self.assertIn("x", info.resolution)

        self.assertGreater(info.duration, 2)


if __name__ == "__main__":
    unittest.main()
