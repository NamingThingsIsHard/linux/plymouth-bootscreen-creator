# Development

Besides the requirements, you need to install the python dependencies.
That's done automatically by [`cargo`][cargo] when building.

## Building

`carg build --bin pbc`

## Editing / Formatting code

The project uses a

### Formatter

[`rustfmt`][rustfmt] formats the code for you and reduces conflicts of style.
It will be checked in the CI.

Run `cargo fmt` and everything will be formatted.

### Linter

[`clippy`][clippy] for linting and reducing well-known, suboptimal rust code.
This is also checked in the CI.

Run `cargo clippy` and fix your errors.

[clippy]: https://github.com/rust-lang/rust-clippy
[rustfmt]: https://github.com/rust-lang/rustfmt
