![Logo](src/resources/images/icon.svg)

# Plymouth Bootscreen Creator (PBC)

A GTK application to help create and preview videos as bootscreens.
It's an ugly WIP, but it works.

## Tested on

 - Opensuse 15.2

# Requirements

 - [Rust&Cargo][rust] (lookup how to install it on your distro)
 - [ffmpeg]
 - [Gtk3] >= 3.24 (lookup how to install it on your system)

### Credits

Original repo with shell script by Eionix: https://github.com/krishnan793/PlymouthTheme-Cat/commits?author=krishnan793

--------------------

See [CONTRIBUTING.md](./CONTRIBUTING.md) to get started with development.

--------------------

![Icon] by Kijawi.

[Gtk3]: https://www.gtk.org/docs/installations/linux/
[ffmpeg]: https://ffmpeg.org/download.html
[rust]: https://www.rust-lang.org/learn/get-started
