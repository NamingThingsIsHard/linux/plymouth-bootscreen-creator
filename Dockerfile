FROM debian:stable-slim as base
ARG WORKDIR
WORKDIR ${WORKDIR:-/app}

RUN apt-get update -qqq \
    && apt-get install -yqqq --no-install-recommends \
        curl ca-certificates \
        ffmpeg \
        gcc \
        libgtk-3-dev \
    # Clean up downloaded packages to save space
    && rm -rf /var/lib/apt/lists/*
RUN echo "install rustup" && curl https://sh.rustup.rs -sSf | sh -s -- -y
ENV PATH="/root/.cargo/bin:$PATH"

# With full debug symbols
FROM base as dev
COPY . ./
RUN cargo build

FROM base as prod
COPY . ./
RUN cargo build --release
